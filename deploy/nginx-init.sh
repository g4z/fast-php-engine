#!/sbin/openrc-run
#
# === DEV NOTES ===
#
# nginx version: nginx/1.18.0
# Usage: nginx [-?hvVtTq] [-s signal] [-c filename] [-p prefix] [-g directives]
#
# Options:
#   -?,-h         : this help
#   -v            : show version and exit
#   -V            : show version and configure options then exit
#   -t            : test configuration and exit
#   -T            : test configuration, dump it and exit
#   -q            : suppress non-error messages during configuration testing
#   -s signal     : send signal to a master process: stop, quit, reopen, reload
#   -p prefix     : set prefix path (default: /var/lib/nginx/)
#   -c filename   : set configuration file (default: /etc/nginx/nginx.conf)
#   -g directives : set global directives out of configuration file

description="Starts nginx service"

# depend() {
#     need net
#     # need localmount
#     # keyword -prefix -lxc -docker
# }

start() {
        ebegin "Starting nginx"
        if [ ! -d /run/nginx ]; then mkdir /run/nginx/; fi
        /usr/sbin/nginx
        eend $?
}

stop() {
    ebegin "Stopping nginx"
    /usr/sbin/nginx -s stop
    eend $?
}

reload() {
    ebegin "Reloading nginx"
    /usr/sbin/nginx -s reload
    eend $?
}

refresh() {
    ebegin "Refresh nginx configuration"
    /usr/sbin/nginx -s reopen
    eend $?
}
