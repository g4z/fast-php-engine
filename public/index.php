<?php declare(strict_types=1);

use hollodotme\FastCGI\Client;
use hollodotme\FastCGI\Requests\PostRequest;
use hollodotme\FastCGI\RequestContents\JsonData;
use hollodotme\FastCGI\SocketConnections\UnixDomainSocket;

include '../vendor/autoload.php';

// error_log('An error occured');

$connection = new UnixDomainSocket(
    '/run/php-fpm-worker.sock'
);

$data = new JsonData([
    'key' => 'value123',
    'something' => [
        'hello' => 'world',
        'this' => [
            'is a test',
        ],
    ],
]);

// create fcgi client
$client = new Client();

// create JSON request
$payload = PostRequest::newWithRequestContent(
    '/srv/workers/example.php',
    $data
);

$id = $client->sendAsyncRequest($connection, $payload);
$response = $client->readResponse($id);

$json = $response->getBody();
$data = json_decode($json);

header("Content-Type: text/json");
echo json_encode($data);
