
# Containerised OpenRC + NGINX + PHP-FPM

Lightweight self-contained PHP server to test adding async processing using a PHP-FPM background work pool. A possible alternative to using a redis queue.

Idea from: Async PHP Requests & Reactive Responses with PHP-FPM
https://www.youtube.com/watch?v=lnFyPbqVIBc

Current state: the web accessible index.php scripts receives a request and calls the example worker via the fastcgi worker pool. the index.php has examples of calling this worker pool syncronously and asyncronously.

The connection between web pool scripts and work pool scripts is using JSON over FastCGI.

HTTP style status error checking should be implemented in the client (index.php)

## Running

```
$ make build
$ make run
# in another terminal...
$ curl -s localhost | jq
```

## Docs

- https://github.com/hollodotme/fast-cgi-client/

- https://wiki.gentoo.org/wiki/OpenRC
- http://big-elephants.com/2013-01/writing-your-own-init-scripts/
- https://www.osetc.com/en/how-to-start-stop-restart-services-on-alpine-linux.html

- http://nginx.org/en/docs

### alpine repositories ( add to /etc/apk/repositories ):

````
http://dl-cdn.alpinelinux.org/alpine/v3.12/main
http://dl-cdn.alpinelinux.org/alpine/v3.12/community
@edge http://dl-cdn.alpinelinux.org/alpine/edge/main
@testing http://dl-cdn.alpinelinux.org/alpine/edge/testing
```

### openrc info

```
    # tell openrc loopback and net are already there, since docker handles the networking
    # echo 'rc_provide="loopback net"' >> /etc/rc.conf && \
    # # no need for loggers
    # sed -i 's/^#\(rc_logger="YES"\)$/\1/' /etc/rc.conf && \
    # # can't get ttys unless you run the container in privileged mode
    # # can't mount tmpfs since not privileged
    # sed -i 's/mount -t tmpfs/# mount -t tmpfs/g' /lib/rc/sh/init.sh && \
    # # can't do cgroups
    # sed -i 's/cgroup_add_service /# cgroup_add_service /g' /lib/rc/sh/openrc-run.sh
```

