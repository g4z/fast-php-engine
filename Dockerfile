FROM alpine:3.12

# use a mirror for now because
# the main repo is having issues
COPY deploy/repositories /etc/apk/repositories

RUN apk --update upgrade && \
    apk --update add \
        openrc \
        nginx \
        php7 \
        php7-fpm \
        php7-json && \
    rm -fr /tmp/* \
        /etc/init.d/* \
        /var/cache/apk/*

# openrc config
RUN sed -i '/tty/d' /etc/inittab
COPY deploy/rc.conf /etc/rc.conf
COPY deploy/php-fpm-init.sh /etc/init.d/php-fpm
COPY deploy/nginx-init.sh /etc/init.d/nginx
RUN rc-update add nginx default && \
    rc-update add php-fpm default && \
    rc-update --update

# php config
COPY deploy/php.ini /etc/php7/php.ini
COPY deploy/php-fpm.conf.ini /etc/php7/php-fpm.conf

# fpm pool config
COPY deploy/fpm-pool-www.conf.ini /etc/php7/php-fpm.d/www.conf
COPY deploy/fpm-pool-worker.conf.ini /etc/php7/php-fpm.d/worker.conf

# nginx config
COPY deploy/nginx.conf /etc/nginx/nginx.conf
COPY deploy/default.conf /etc/nginx/conf.d/default.conf

# copy code into container
# COPY . /srv

CMD ["/sbin/init"]
