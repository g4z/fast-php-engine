<?php declare(strict_types=1);

$input = file_get_contents('php://input');
$json = json_decode($input, true);

$data = json_encode([
    "status" => "ok",
    "message" => "you suck",
    "input" => $json,
    "debug" => [
        '_REQUEST' => $_REQUEST,
        '_SERVER' => $_SERVER,
        '_ENV' => $_ENV,
    ]
]);

header("Content-Type: text/json");
echo $data;
