
build:
	@docker build -t fast-php-nginx .

rebuild:
	@docker build --no-cache -t fast-php-nginx .

shell:
	@docker run \
		-v /sys/fs/cgroup:/sys/fs/cgroup:ro \
		--tmpfs /run \
		--rm -it \
		fast-php-nginx:latest \
		sh

run:
	@docker run \
		-v /sys/fs/cgroup:/sys/fs/cgroup:ro \
		-v $$(pwd):/srv:ro \
		--tmpfs /run \
		--rm \
		-it \
		-p 80:80 \
		fast-php-nginx:latest
